import {Component, OnInit} from '@angular/core';
import {PromoterService} from "../services/promoter.service";
import {ToastrService} from "ngx-toastr";
import {NotificationService} from "../services/notification.service";
import {Router} from '@angular/router'

@Component({
    selector: 'app-promoter',
    templateUrl: './promoter.component.html',
    styleUrls: ['./promoter.component.scss']
})
export class PromoterComponent implements OnInit {
    rows: any = [];
    totalSize;
    model;
    currentPage = 0;
    pageSize = 5;
    display = 'none';
    display1 = 'none';
    promoterName;
    influencerTotalCount = 0;
    influencerData = [];
    influencerUrl;
    Url;
    decription;
    emailId;
    btndisable = true;
    editBlock = false;
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];
    editData: any = {};
    iData: any = [];
    pData: any = {};
    totalidata;

    constructor(private promoterService: PromoterService,
                private notificationService: NotificationService,
                private router: Router) {
    }

    ngOnInit() {
        this.getCategory(this.currentPage, this.pageSize);
    }

    getCategory(pageNumber, pageSize) {
        this.promoterService.getdata(pageNumber, pageSize)
            .subscribe((response) => {
                    console.log('response', response)
                    if (!response.error) {
                        this.rows = response.data.results;
                        this.totalSize = response.data.totalRecords;
                        let Count = 0;
                        response.data.results.filter((x, i)=>{
                            if(i!==0) {
                                response.data.results[i - 1].influencerCount = Count;
                            }
                            Count = 0;
                            x.influencer.filter((y)=>{
                                Count += y.influencerCount
                            })
                            if((i) === (response.data.results).length -1){
                                response.data.results[i].influencerCount = Count;
                            }
                        });

                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    addPromoter() {
        this.display = 'block';
    }

    signup(data) {
        console.log('data', data)
        this.promoterService.update(data)
            .subscribe((response) => {
                    console.log('response', response)
                    if (!response.error) {
                        this.notificationService.showNotification(response.message, 'success');
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
        this.router.navigate(['/signup'])
    }

    dashboard(data) {
        console.log('data', data)
        this.promoterService.Influencerupdate(data.influencerId)
            .subscribe((response) => {
                    console.log('response', response)
                    if (!response.error) {
                        this.notificationService.showNotification(response.message, 'success');
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
        // this.router.navigate(['/promoters'])
        window.location.reload();
    }

    influencerpopup(data) {
        this.pData = data;
        this.iData = data.influencer;
        this.totalidata = data.influencer.length;
        this.display1 = 'block';
    }

    getinfluencer(data) {
        this.iData = data.influencer;
        this.totalidata = data.influencer.length;
    }

    addData(influencerUrl) {
        let json = {
            promoterId: this.pData.promoterSchemaId,
            influencerUrl: influencerUrl
        }
        this.promoterService.Promoterupdate(json)
            .subscribe((response) => {
                    console.log('response', response)
                    if (!response.error) {
                        this.notificationService.showNotification(response.message, 'success');
                        this.iData = response.data.influencer;
                        this.totalidata = response.data.influencer.length;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
        this.influencerUrl = ''
    }; // end of addData

    onCloseHandled() {
        this.display = 'none';
        this.promoterName = null;
        this.Url = null;
        this.decription = null;
        this.emailId = null;
    }

    onCloseofInfluencerHandled() {
        this.display1 = 'none';
    }



    checkValue() {
        if ((this.promoterName === null || this.promoterName === undefined || this.promoterName === '') || (this.Url === null || this.Url === undefined || this.Url === '') && (this.decription === null || this.decription === undefined || this.decription === '') && (this.emailId === null || this.emailId === undefined || this.emailId === '')) {
            this.btndisable = true;
        } else {
            this.btndisable = false;
        }
    }

    saveCategory() {
        this.display = 'none';
        if (this.promoterName && this.Url && this.decription && this.emailId && this.editBlock === false) {
            let data = {
                promoterName: this.promoterName,
                Url: this.promoterName,
                decription: this.decription,
                emailId: this.emailId,
            };
            console.log('dataratata', data)
            this.promoterService.create(data)
                .subscribe((response) => {
                        if (!response.error) {
                            this.notificationService.showNotification(response.message, 'success');
                            this.getCategory(this.currentPage, this.pageSize);
                            this.promoterName = null;
                            this.Url = null;
                            this.decription = null;
                            this.emailId = null;
                        } else {
                            this.notificationService.showNotification(response.message, 'danger')
                        }
                    },
                    error1 => {
                        console.log(error1);
                        this.notificationService.showNotification(error1, 'danger');
                    })
        }
    }

    editCategory(data) {
        console.log('edit', data);
        this.display = 'block';
        this.editData = data;
        this.promoterName = data.promoterName;
        this.editBlock = true;
        this.btndisable = false;
    }

    // deleteCategory(data) {
    //   console.log('delete', data.categoryId);
    //   let value = {
    //     categoryId: data.categoryId
    //   };
    //   this.quizService.deleteCategory(value)
    //       .subscribe((response) => {
    //             if (!response.error) {
    //               this.notificationService.showNotification(response.message, 'success');
    //               this.getCategory(this.currentPage, this.pageSize);
    //             } else {
    //               this.notificationService.showNotification(response.message, 'danger')
    //             }
    //
    //           },
    //           error1 => {
    //             console.log(error1);
    //             this.notificationService.showNotification(error1, 'danger');
    //           })
    // }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getCategory(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getCategory(e.offset, e.pageSize);
    }
}

