import { Component, OnInit } from '@angular/core';
import { UserService } from "../services/user.service";
import {Router} from "@angular/router";
import {NotificationService} from "../services/notification.service";


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    user: any = {};

  constructor(private userService: UserService,
              private notificationService: NotificationService,
              private router: Router) { }

  ngOnInit() {
  }
  
  userRegistration() {
      console.log('this.user',this.user)
    this.userService.signup(this.user)
        .subscribe((response) => {
            console.log('responseee',response)
          if (!response.error) {
            localStorage.setItem('token', response.data.authToken);
            this.notificationService.showNotification(response.message, 'success');
              this.router.navigate(['/dashboard']);

          } else {
            this.notificationService.showNotification(response.message, 'danger');
          }
        });
  }
}
