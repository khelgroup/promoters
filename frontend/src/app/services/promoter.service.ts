import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class PromoterService {

    api = environment.apiUrl;
    constructor(private http: HttpClient) { }

    create(data) {
        return this.http.post<any>(this.api + '/promoter/create', data);
    }
    getdata(pageNumber, pagesize) {
        return this.http.get<any>(this.api + '/promoter/list?pg=' + pageNumber + '&pgSize=' + pagesize);
    }

    getInfluencerdata(promoterId) {
        return this.http.get<any>(this.api + '/promoter/influencerlist?promoterId=' + promoterId);
    }
    update(promoterId) {
        // @ts-ignore
        return this.http.post<any>(this.api + '/promoter/update?promoterId=' + promoterId);
    }

    Influencerupdate(influencerId) {
        // @ts-ignore
        return this.http.post<any>(this.api + '/promoter/Influencerupdate?influencerId=' + influencerId);
    }
    Promoterupdate(data) {
        // @ts-ignore
        return this.http.post<any>(this.api + '/promoter/updatepromoter', data);
    }
}
