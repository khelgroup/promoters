const mongoose = require('mongoose');
const shortid = require('shortid');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
const {isNullOrUndefined} = require('util');
const check = require('../libs/checkLib')

var faker = require('faker');

/* Models */
const Promoter = mongoose.model('Promoters');

// create Promoters function
let createPromoter = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.promoterName && req.body.Url && req.body.decription && req.body.emailId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "promoterName,url, description and email missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let createPromoters = () => {
        console.log("createPromoters");
        return new Promise((resolve, reject) => {
            let body = {};
            body['promoterSchemaId'] = shortid.generate();
            body['promoterName'] = req.body.promoterName;
            body['url'] = req.body.Url;
            body['decription'] = req.body.decription;
            body['emailId'] = req.body.emailId;
            body['createdOn'] = new Date();
            console.log('bodyy', body)
            Promoter.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create Promoters", "promoterController => createPromoters()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    resolve(finalObject);
                }
            });
        });
    } // end of createPromoters function

    validatingInputs()
        .then(createPromoters)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Promoter Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of create Promoters function

// update Promoter function
let updatePromoter = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.influencerUrl && req.body.promoterId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "PromoterId and InfluencerUrl  missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let getPromoters = () => {
        console.log("getPromoters");
        return new Promise((resolve, reject) => {
            Promoter.findOne({promoterSchemaId: req.body.promoterId})
                .select('-__v -_id')
                .exec((err, promoterDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve promoter", "promoterController => findPromoter()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve promoter", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(promoterDetails)) {
                        logger.error("No promoter found", "promoterController => findPromoter()", 5);
                        let apiResponse = response.generate(true, "No promoter found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(promoterDetails);
                    }
                });
        });
    } // end of getPromoters function

    let updateePromoters = (promoterDetails) => {
        console.log("updateePromoters");
        return new Promise((resolve, reject) => {
            const json = {
                'influencerId': shortid.generate(),
                'influencerUrl': req.body.influencerUrl,
                'influencerCount': 0
            }
            promoterDetails.influencer.push(json);
            Promoter.findOneAndUpdate({promoterSchemaId: promoterDetails.promoterSchemaId}, promoterDetails, {new: true})
                .select('-__v -_id')
                .exec((err, promoter) => {
                    if (err) {
                        logger.error("Failed to retrieve Promoter data", "promoterController => updatePromoter()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve Promoter data", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(promoter)) {
                        logger.error("Failed to retrieve Promoter data", "promoterController => updatePromoter()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve Promoter data", 500, null);
                        reject(apiResponse);
                    } else {
                        logger.info("Promoter found", "promoterController => updatePromoter()", 10);
                        resolve(promoter);
                    }
                })
        });
    } // end of updateePromoters function

    validatingInputs()
        .then(getPromoters)
        .then(updateePromoters)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Added Influencer Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of update Promoters function

// get Promoter List function
let getPromoterList = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findPromoter = (data) => {
        console.log("findPromoter");
        return new Promise((resolve, reject) => {
            Promoter.find({}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: 1}
            })
                .select('-__v -_id')
                .exec((err, promoterDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve promoter", "promoterController => findPromoter()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve promoter", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(promoterDetails)) {
                        logger.error("No promoter found", "promoterController => findPromoter()", 5);
                        let apiResponse = response.generate(true, "No promoter found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: promoterDetails
                        };
                        resolve(final);
                    }
                });
        });
    } // end of findPromoter Functions

    let totalPromoter = (final) => {
        console.log("totalPlayers");
        return new Promise((resolve, reject) => {
            Promoter.find().count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve payers", "playerController => totalPlayers()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve payers", 500, null);
                    reject(apiResponse);
                } else {
                    final['totalRecords'] = cnt;
                    resolve(final);
                }
            });
        });
    } // end of total Promoter function

    getSizeLimit()
        .then(findPromoter)
        .then(totalPromoter)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Player List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Promoter List function

// get Influencer List function
let getInfluencerList = (req, res) => {

    let findPromoter = () => {
        console.log("findPromoter");
        return new Promise((resolve, reject) => {
            Promoter.findOne({promoterSchemaId: req.query.promoterId}, {}, {})
                .select('-__v -_id')
                .exec((err, promoterDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve promoter", "promoterController => findPromoter()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve promoter", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(promoterDetails)) {
                        logger.error("No promoter found", "promoterController => findPromoter()", 5);
                        let apiResponse = response.generate(true, "No promoter found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: promoterDetails.influencer
                        };
                        resolve(final);
                    }
                });
        });
    } // end of findPromoter Functions

    let totalPromoter = (final) => {
        console.log("totalPlayers");
        return new Promise((resolve, reject) => {
            Promoter.find().count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve payers", "playerController => totalPlayers()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve payers", 500, null);
                    reject(apiResponse);
                } else {
                    final['totalRecords'] = cnt;
                    resolve(final);
                }
            });
        });
    } // end of total Promoter function

    findPromoter()
        .then(totalPromoter)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Influencer List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Influencer List function

// update Count
let updateCount = (req, res) => {

    let updatePromoter = () => {
        console.log("updatePromoter");
        return new Promise((resolve, reject) => {
            if (req.query.promoterId) {
                Promoter.findOneAndUpdate({promoterSchemaId: req.query.promoterId}, {
                    $inc: {
                        totalCount: 1
                    }
                }, {new: true})
                    .select('-__v -_id')
                    .exec((err, promoter) => {
                        if (err) {
                            logger.error("Failed to retrieve Promoter data", "promoterController => updatePromoter()", 5);
                            let apiResponse = response.generate(true, "Failed to retrieve Promoter data", 500, null);
                            reject(apiResponse);
                        } else if (check.isEmpty(promoter)) {
                            logger.error("Failed to retrieve Promoter data", "promoterController => updatePromoter()", 5);
                            let apiResponse = response.generate(true, "Failed to retrieve Promoter data", 500, null);
                            reject(apiResponse);
                        } else {
                            logger.info("Promoter found", "promoterController => updatePromoter()", 10);
                            resolve();
                        }
                    })
            } else {
                logger.info("Successfull", "promoterController => updatePromoter()", 10);
                resolve();
            }

        });
    } // end of updatePromoter

    updatePromoter()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Update successful!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of update Player in function

// update Influencer Count

let updateInfluencerCount = (req, res) => {
    let updateInfluencer = () => {
        console.log("updateInfluencer", req.query.influencerId);
        return new Promise((resolve, reject) => {
            if (req.query.influencerId) {
                Promoter.findOneAndUpdate({'influencer.influencerId': req.query.influencerId}, {
                    $inc: {
                        'influencer.$.influencerCount': 1
                    }
                }, {new: true})
                    .select('-__v -_id')
                    .exec((err, promoter) => {
                        console.log('promotor', promoter)
                        if (err) {
                            logger.error("Failed to retrieve influencer data", "promoterController => updatePromoter()", 5);
                            let apiResponse = response.generate(true, "Failed to retrieve influencer data", 500, null);
                            reject(apiResponse);
                        } else if (check.isEmpty(promoter)) {
                            logger.error("Failed to retrieve Promoter data", "promoterController => updatePromoter()", 5);
                            let apiResponse = response.generate(true, "Failed to retrieve influencer data", 500, null);
                            reject(apiResponse);
                        } else {
                            logger.info("Promoter found", "promoterController => updatePromoter()", 10);
                            console.log('promotor', promoter)
                            resolve();
                        }
                    })
            } else {
                logger.info("Successfull", "promoterController => updatePromoter()", 10);
                resolve();
            }

        });
    } // end of updateInfluencer

    updateInfluencer()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Update successful!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of update Influencer in function


module.exports = {
    createPromoter: createPromoter,
    getPromoterList: getPromoterList,
    updatePromoter: updatePromoter,
    updateCount: updateCount,
    updateInfluencerCount: updateInfluencerCount,
    getInfluencerList: getInfluencerList,
}// end exports