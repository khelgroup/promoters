const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
const validateInput = require('../libs/paramsValidationLib')
const check = require('../libs/checkLib')

/* Models */
const UserModel = mongoose.model('User')


// start user signup function 

let signUp = (req, res) => {
    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.firstName && req.body.lastName && req.body.email && req.body.mobileNumber && req.body.password && req.body.confirmpassword) {
                if(req.body.password === req.body.confirmpassword){
                    resolve(req);
                }else {
                    let apiResponse = response.generate(true, "password and confirmpassword Doesn't match", 400, null);
                    reject(apiResponse);
                }
            } else {
                let apiResponse = response.generate(true, "firstName, lastName, email, mobileNumber, password missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let createUsers = () => {
        console.log("createPlayers");
        return new Promise((resolve, reject) => {
            let body = {};
            body['userId'] = shortid.generate();
            body['firstName'] = req.body.firstName;
            body['lastName'] = req.body.lastName;
            body['email'] = req.body.email;
            body['mobileNumber'] = req.body.mobileNumber;
            body['password'] = req.body.password;
            body['createdOn'] = new Date();
            UserModel.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create user", "userController => createUsers()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createUsers function

    validatingInputs()
        .then(createUsers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Player Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
}// end user signup function 

// start of login function 
let loginFunction = (req, res) => {

}


// end of the login function 


let logout = (req, res) => {

} // end of the logout function.


module.exports = {

    signUp: signUp,
    loginFunction: loginFunction,
    logout: logout

}// end exports