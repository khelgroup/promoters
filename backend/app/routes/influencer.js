const express = require('express');
const router = express.Router();
const promoterController = require("../controllers/influencerController");
// const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig")

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/promoter`;

    // defining routes.

    app.post(`${baseUrl}/create`, promoterController.createPromoter);

    app.post(`${baseUrl}/updatepromoter`, promoterController.updatePromoter);

    app.get(`${baseUrl}/list`, promoterController.getPromoterList);

    app.get(`${baseUrl}/influencerlist`, promoterController.getInfluencerList);

    app.post(`${baseUrl}/update`, promoterController.updateCount);

    app.post(`${baseUrl}/Influencerupdate`, promoterController.updateInfluencerCount);
}
