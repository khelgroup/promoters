'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let promoterSchema = new Schema({
    promoterSchemaId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    promoterName: {
        type: String,
        default: ''
    },
    url: {
        type: String,
        default: ''
    },
    influencer: { type : Array , default : [] },
    decription: {
        type: String,
        default: ''
    },
    emailId: {
        type: String,
        default: ''
    },
    totalCount: {
        type: Number,
        default: 0
    },
    createdOn: {
        type: Date,
        default: ''
    },
}, { versionKey: false  })


mongoose.model('Promoters', promoterSchema);
